Source: golang-github-syncthing-notify
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Alexandre Viau <aviau@debian.org>,
           Jai Flack <jflack@disroot.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-golang-x-sys-dev
Standards-Version: 4.6.0
Homepage: https://github.com/syncthing/notify
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-syncthing-notify
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-syncthing-notify.git
XS-Go-Import-Path: github.com/syncthing/notify
Testsuite: autopkgtest-pkg-go
Rules-Requires-Root: no

Package: golang-github-syncthing-notify-dev
Architecture: all
Depends: ${misc:Depends},
         golang-golang-x-sys-dev
Description: File system event notification library on steroids
 Package notify implements access to filesystem events.
 .
 Notify is a high-level abstraction over filesystem watchers
 like inotify, kqueue, FSEvents, FEN or ReadDirectoryChangesW.
 Watcher implementations are split into two groups: ones that
 natively support recursive notifications (FSEvents and
 ReadDirectoryChangesW) and ones that do not (inotify, kqueue,
 FEN).
 .
 This is a fork by the Syncthing project.
